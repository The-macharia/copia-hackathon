# -*- coding: utf-8 -*-

from odoo.tests.common import TransactionCase
from odoo.exceptions import ValidationError


class TestResPartnerChild(TransactionCase):

    def setUp(self):
        super().setUp()
        self.partner = self.env['res.partner'].create({
            'name': 'Test partner 001',
            'has_children': 'yes',
            'no_of_children': 2,
        })
        self.partner_child = self.env['res.partner.child.create'].create({
            'name': 'Test Partner Child 001',
            'age': 10,
            'gender': 'female'
        })

    def test_partner_child_equals_no_of_children(self):
        child_ids = [(0, 0, {
            'name': f'Test Partner Child 00{i}',
            'age': 3 * i,
            'gender': 'female'
        }) for i in range(1, 4)]
        with self.assertRaises(ValidationError):
            self.partner.write({'children_ids': child_ids})
        child_ids.pop()
        res = self.partner.write({'children_ids': child_ids})
        self.assertTrue(res)
        self.assertEqual(len(self.partner.children_ids), len(child_ids))

    def test_update_years_cron(self):
        child_age = self.partner_child.age
        self.env['res.partner'].update_children_age()
        self.assertEqual(self.partner_child.age, child_age + 1)
