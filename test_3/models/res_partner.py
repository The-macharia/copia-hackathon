# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import ValidationError


class ResPartner(models.Model):
    _inherit = 'res.partner'

    has_children = fields.Selection(string='Has Children?', selection=[('yes', 'Yes'), ('no', 'No')], default='no')
    no_of_children = fields.Integer(string='No. of children')
    children_ids = fields.One2many('res.partner.child', 'parent_id', string='Children')

    @api.constrains('no_of_children', 'children_ids')
    def _constrains_children(self):
        for rec in self:
            if len(rec.children_ids) > rec.no_of_children:
                raise ValidationError(_('A partner cannot be assigned more children than their No of Children'))


class ResPartnerChild(models.Model):
    _name = 'res.partner.child'
    _description = 'Partner children'

    name = fields.Char(string='Name', required=True)
    age = fields.Integer(string='Age', required=True)
    gender = fields.Selection(string='Gender', selection=[(
        'female', 'Female'), ('male', 'Male'), ('other', 'Other')], required=True)
    parent_id = fields.Many2one('res.partner', string='Parent')

    @api.model
    def update_children_age(self):
        children = self.env['res.partner.child'].search([])
        for child in children:
            child.write({'age': child.age + 1})
