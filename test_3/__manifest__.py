# -*- coding: utf-8 -*-
{
    'name': "Test 3: Partner Children",

    'summary': """Practical test no.3""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Eric Macharia",
    'website': "http://www.yourcompany.com",
    'license': 'LGPL-3',

    'category': 'Uncategorized',
    'version': '15.0.0.1.0',

    # we specifically need the sales module to enable customer rank in contacts
    'depends': ['sale_management'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/cron.xml',
        'views/views.xml',
    ]
}
