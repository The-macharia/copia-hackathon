# Copia Hackathon



## Getting started
To be able to run this project the follwoing assumption have been made:

- You're running Python version greater that 3.7
- You have a clean installation for odoo 15 (both community and enterprise editions should work okay.)
- You have postgres installed.
- You're running these tests on a unix based operating system.

Clone this repository onto your target machine and run the command below to install the project dependencies
`pip3 install -r requirements.txt`

# TEST 1 (Mandatory Test)
While not mandatory, the test assumes that the script is being run in a local environment with default postgres configurations.
If that is not the case, you can set environment variables to connect to your target postgres instance
The environments are:

- HACKATHON_HOST defaults to localhost

- HACKATHON_DB defaults to users

- HACKATHON_DB_USER defaults to postgres

- HACKATHON_PASSWORD defaults to postgres

- HACKATHON_PORT defaults to 5432

Run the command such as `export HACKATHON_HOST=localhost` to set the environment variable from the default.
For this test, after cloning the repository, navigate to the folder `test_1` and there run the command below 
to create a testing database

`python3 reset_db.py`

After this run the following command to execute the script
`python3 main.py`

# TEST 2 (Mandatory Test)
For this test, after cloning the repository, navigate to the folder `test_2` and there run the command below 
to execute the script
`python3 main.py`

# For the odoo specific Tests (Question No. 3 and No. 5) (Optional)
### The easiest way to run the odoo challenges is using docker.
This method however assumes you have docker and docker-compose installed.
After cloing the repo, navigate to the root folder of the project and run the command 
`docker-compose up`
On the very first run, a new db copia-hackathon will be automatically created with the default logins
(username: admin, password: admin)
Navigate to the url `localhost:8069` and install the modules
- Test 3: Partner Children
- Test 5: Vehicle Tracker

### Without docker
Using your local installation of odoo 15: 
You can either add the modules `test_3` and `test_5` to your addons directory, or you can link this directory to your odoo configuration file.

After the above, restart your odoo server and head on to your browser and if all odoo parameters are default add the url `localhost:8069`

If no new database exists, this page will prompt you to create a new database and after this, headon to Apps menu and intall the apps:
- Test 3: Partner Children
- Test 5: Vehicle Tracker

## Testing
To run the tests for this challenge, stop your odoo server and start odoo server with tests enabled
ie: // `./odoo-bin -d <database> --no-http --test-enable -i test_3`

## Happy Hacking!
