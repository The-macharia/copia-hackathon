# -*- coding: utf-8 -*-

from odoo.tests.common import TransactionCase
from datetime import datetime


class TestVehicleRange(TransactionCase):

    def setUp(self):
        super().setUp()

        self.type_id = self.env['vehicle.type'].create({'name': 'Test Type 001'})
        self.make_id = self.env['vehicle.make'].create({'name': 'Test Make 001'})
        self.partner_id = self.env['res.partner'].create({'name': 'Test Partner 001'})
        self.fuel_type_id = self.env['vehicle.fuel.type'].create({'name': 'Test Fuel Type 001'})

    def test_create_vehicle_tracker(self):
        self.vehicle_tracker = self.env['vehicle.tracker'].create({
            'name': 'Test Name 001',
            'type_id': self.type_id.id,
            'make_id': self.make_id.id,
            'partner_id': self.partner_id.id,
            'fuel_type_id': self.fuel_type_id.id,
            'chasis_number': 'Test chasis_number 001',
            'expiry_date': datetime.today(),
            'fuel_consumption': 1000,
        })
        self.assertTrue(self.vehicle_tracker)
        self.assertEqual(self.vehicle_tracker.partner_id.id, self.partner_id.id)
        self.assertEqual(self.vehicle_tracker.type_id.id, self.type_id.id)
        self.assertEqual(self.vehicle_tracker.make_id.id, self.make_id.id)
        self.assertEqual(self.vehicle_tracker.fuel_type_id.id, self.fuel_type_id.id)
        self.assertFalse(self.vehicle_tracker.phone)
