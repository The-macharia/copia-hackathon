# -*- coding: utf-8 -*-
{
    'name': "Test 5: Vehicle Tracker",

    'summary': """Practical test no.5""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Eric Macharia",
    'website': "http://www.yourcompany.com",
    'license': 'LGPL-3',

    'category': 'Uncategorized',
    'version': '15.0.0.1.0',


    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/menu.xml',
    ]
}
