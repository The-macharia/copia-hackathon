# -*- coding: utf-8 -*-


from odoo import fields, models


class VehicleTracker(models.Model):
    _name = 'vehicle.tracker'
    _description = 'Vehicle tracker'

    name = fields.Char(string='Vehicle Registration', required=True)
    type_id = fields.Many2one('vehicle.type', string='Vehicle Type', required=True)
    make_id = fields.Many2one('vehicle.make', string='Vehicle Make', required=True)
    chasis_number = fields.Char(string='Chasis Number', required=True)
    partner_id = fields.Many2one('res.partner', string='Supplier', required=True)
    expiry_date = fields.Date(string='Insurance Expiry Date', required=True)
    fuel_type_id = fields.Many2one('vehicle.fuel.type', string='Fuel Type', required=True)
    fuel_consumption = fields.Float(string='Fuel Consumption', required=True)
    tag = fields.Char(string='Vehicle Tag', readonly=True)
    label_ids = fields.Many2many('vehicle.label', string='Vehicle Labels')
    status = fields.Boolean(string='Status of Vehicle')
    company_id = fields.Many2one('res.company', string='Company')
    node = fields.Integer(string='Node ID')
    node_type = fields.Integer(string='Node Type ID')
    data_type = fields.Char(string='Type')
    fleet_number = fields.Char(string='Fleet Number')
    description = fields.Text('Description')
    mit = fields.Char(string='Mit')
    serial = fields.Char(string='Serial Number')
    phone = fields.Char(string='Cellphone Number')
    symbol = fields.Integer(string='Vehicle Symbol')
    cost_centre = fields.Integer(string='Cost Centre')
    last_track = fields.Datetime(string='Last Tracked At', readonly=True)


class VehicleType(models.Model):
    _name = 'vehicle.type'
    _description = 'Vehicle type'

    name = fields.Char(string='Name')


class VehicleMake(models.Model):
    _name = 'vehicle.make'
    _description = 'Vehicle make'


class VehicleFuelType(models.Model):
    _name = 'vehicle.fuel.type'
    _description = 'Vehicle fuel type'

    name = fields.Char(string='Name')


class VehicleLabel(models.Model):
    _name = 'vehicle.label'
    _description = 'Vehicle label'

    name = fields.Char(string='Name')
