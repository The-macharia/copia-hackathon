import csv


def run(file):
    day = 1
    max_spread = 1
    with open(file, 'r') as f:
        data = [line.strip().split() for line in f.readlines()]

    data = map(lambda a: a[:3], filter(lambda d: d, data[1:-1]))
    for line in data:
        dy, mxt, mnt = line
        spread = int(mxt.replace('*', '')) - int(mnt.replace('*', ''))
        if spread > max_spread:
            max_spread = spread
            day = dy
    print(day, max_spread)


if __name__ == '__main__':
    file = 'weather.dat'
    run(file)
