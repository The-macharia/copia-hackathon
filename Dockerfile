FROM odoo:15.0
USER root
ENV DEBIAN_FRONTEND=noninteractive \
    LANG=C.UTF-8

RUN apt-get update

RUN apt-get install --no-install-recommends -y software-properties-common python3-pip \
     python3-psycopg2 libgeos-dev build-essential libjpeg-dev python3-dev python3-watchdog \
     python3-setuptools python3-wheel procps coffeescript && rm -f /var/lib/apt/lists/*.*


COPY . /mnt
COPY ./config /etc/odoo
RUN pip3 install -r /mnt/requirements.txt
USER odoo
