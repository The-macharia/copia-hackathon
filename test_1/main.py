import csv
import json
import os
from random import random

import psycopg2
import requests


def read(file):
    with open(file, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            yield row


def db_insert(row_id, raw, response):
    try:
        response = json.dumps(response)
        cr.execute(f'''INSERT INTO users(id, first_name, second_name, age, response)
            VALUES ({row_id}, {raw['first_name']}, {raw['second_name']}, {raw['Age']}, {str(response)});
        ''')
    except Exception as e:
        print(e)


def run(csv_file):
    for index, row in enumerate(read(csv_file), start=1):
        try:
            res = requests.post(url, data=json.dumps(row), headers=headers)
            res.raise_for_status()
            res = json.loads(res.text).get('json')
            row_id = int(index * random() * 100_000)
            db_insert(row_id, row, res)
        except Exception as e:
            print(e)


if __name__ == '__main__':
    csv_file = 'users.csv'
    conn = psycopg2.connect(
        host=os.environ.get('HACKATHON_HOST', 'localhost'),
        database=os.environ.get('HACKATHON_DB', 'users'),
        user=os.environ.get('HACKATHON_DB_USER', 'postgres'),
        password=os.environ.get('HACKATHON_PASSWORD', 'postgres'),
        port=os.environ.get('HACKATHON_PORT', 9011)
    )

    cr = conn.cursor()
    url = 'http://httpbin.org/post'
    headers = {'Content-Type': 'application/json'}
    run(csv_file)
    conn.commit()
    cr.close()
