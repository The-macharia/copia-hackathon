import os
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


host = os.environ.get('HACKATHON_HOST', 'localhost')
database = os.environ.get('HACKATHON_DB', 'users')
user = os.environ.get('HACKATHON_DB_USER', 'postgres')
password = os.environ.get('HACKATHON_PASSWORD', 'postgres')
port = os.environ.get('HACKATHON_PORT', 5432)

conn = psycopg2.connect(f'host={host} user={user} password={password} port={port}')
conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

cr = conn.cursor()
db = database

cr.execute(f"drop database if exists {database};")
cr.execute(f"create database {database};")

db_conn = psycopg2.connect(host=host, database=database, user=user, password=password, port=port)
db_cr = db_conn.cursor()
db_cr.execute('''create table if not exists users(
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(255) NOT NULL,
    second_name VARCHAR(255) NOT NULL,
    age INTEGER NOT NULL,
    response JSONB NOT NULL
)''')

db_conn.commit()
db_cr.close()

cr.close()
conn.commit()
